package com.example.conor.sidelinestats;

import android.widget.Button;

import com.androidplot.xy.XYPlot;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by conor on 24/03/15.
 */
public class GraphActivityTest extends ActivityInstrumentationTestCase2<GraphActivity> {
    private GraphActivity graphActivity;
    private XYPlot plot;

    public GraphActivityTest(){
        super(GraphActivity.class);
    }

    protected void setUp() throws Exception{

        plot = (XYPlot)graphActivity.findViewById(R.id.mySimpleXYPlot);
    }

    public void testLayout(){
        assertNotNull(plot);

    }
}
