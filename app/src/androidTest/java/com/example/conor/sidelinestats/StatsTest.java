package com.example.conor.sidelinestats;

import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.TestCase;

/**
 * Created by conor on 18/03/15.
 */
public class StatsTest extends TestCase{
    @Override
    protected void setUp() throws Exception{
        super.setUp();
    }

    @SmallTest
    public void testSetAndGetMode(){
        Stats s = new Stats();
        s.setMode(2);
        int mode = s.getMode();
        assertEquals(2,mode);
    }

    @SmallTest
    public void testGetMetricMethods(){
        Stats s = new Stats();
        int [] metric = s.getConversions();
        assertNotNull(metric);
    }

    @SmallTest
    public void testSetAndGetMostRecentInput(){
        Stats s = new Stats();
        s.setMostRecentInput(5);
        int num = s.getMostRecentInput();
        assertEquals(5,num);
    }

}
