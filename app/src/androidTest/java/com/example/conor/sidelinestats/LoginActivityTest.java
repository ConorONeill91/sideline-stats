package com.example.conor.sidelinestats;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Instrumentation;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by conor on 18/03/15.
 */

public class LoginActivityTest extends ActivityInstrumentationTestCase2<LoginActivity> {

    private LoginActivity loginActivity;


    private Button button;

    public LoginActivityTest(){
        super(LoginActivity.class);
    }

    private Intent mLaunchIntent;

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        button = (Button) loginActivity.findViewById(R.id.submit);
    }

    @SmallTest
    public void testLayout(){
        assertNotNull(button);
        assertEquals("Incorrect title", "Submit", button.getText());
    }




}
