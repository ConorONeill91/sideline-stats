package com.example.conor.sidelinestats;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;

/**
 * Created by conor on 18/03/15.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    private Button button;
    private MainActivity mainActivity;

    public MainActivityTest(){
        super(MainActivity.class);
    }

    protected void setUp() throws Exception{

        button = (Button)mainActivity.findViewById(R.id.gk_button);
    }
    @SmallTest
    public void testLayout(){
        assertNotNull(button);
        assertEquals("Incorrect naming convention","GK",button.getText());
    }
}
