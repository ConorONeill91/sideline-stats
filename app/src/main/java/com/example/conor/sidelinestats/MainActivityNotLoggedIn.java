package com.example.conor.sidelinestats;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivityNotLoggedIn extends ActionBarActivity {

    private Stats playerStats;
    private boolean loggedIn;
    private ArrayList<int[]> metrics = new ArrayList<>();
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playerStats = new Stats();
        setContentView(R.layout.activity_main_activity_not_logged_in);

        tv = (TextView) findViewById(R.id.error_box);
        Intent intent = getIntent();
        loggedIn = intent.getBooleanExtra("Logged In", false);
        setTitle("SideLine Stats - Not Logged In");

        updateScore();


    }

    public void incCounter(View v) {
        switch (v.getId()) {
            case R.id.gk_button:
                playerStats.inc(playerStats.getMode(), 0);
                break;
            case R.id.lcb_button:
                playerStats.inc(playerStats.getMode(), 1);
                break;
            case R.id.fb_button:
                playerStats.inc(playerStats.getMode(), 2);
                break;
            case R.id.rcb_button:
                playerStats.inc(playerStats.getMode(), 3);
                break;
            case R.id.lhb_button:
                playerStats.inc(playerStats.getMode(), 4);
                break;
            case R.id.cb_button:
                playerStats.inc(playerStats.getMode(), 5);
                break;
            case R.id.rhb_button:
                playerStats.inc(playerStats.getMode(), 6);
                break;
            case R.id.mf1_button:
                playerStats.inc(playerStats.getMode(), 7);
                break;
            case R.id.mf2_button:
                playerStats.inc(playerStats.getMode(), 8);
                break;
            case R.id.lhf_button:
                playerStats.inc(playerStats.getMode(), 9);
                break;
            case R.id.cf_button:
                playerStats.inc(playerStats.getMode(), 10);
                break;
            case R.id.rhf_button:
                playerStats.inc(playerStats.getMode(), 11);
                break;
            case R.id.lcf_button:
                playerStats.inc(playerStats.getMode(), 12);
                break;
            case R.id.ff_button:
                playerStats.inc(playerStats.getMode(), 13);
                break;
            case R.id.rcf_button:
                playerStats.inc(playerStats.getMode(), 14);
                break;
        }

       updateScore();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity_not_logged_in, menu);
        return true;


    }

    @Override // ActionBar button click handling
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.undo_button:
                playerStats.undo(playerStats.getMostRecentInput());
                updateScore();
                return true;
            case R.id.graph_button:
                Intent intent = new Intent(this, GraphActivity.class);
                intent.putExtra("Conversions", playerStats.getConversions());
                intent.putExtra("Turnovers", playerStats.getTurnovers());
                intent.putExtra("Goals", playerStats.getGoals());
                intent.putExtra("Points", playerStats.getPoints());
                intent.putExtra("Wides", playerStats.getWides());
                intent.putExtra("Tackles", playerStats.getTackles());
                startActivity(intent);
                return true;
            case R.id.conversion_button:
                playerStats.setMode(1);
                setTitle("Current Mode: Conversions");
                return true;
            case R.id.turnover_button:
                playerStats.setMode(2);
                setTitle("Current Mode: Turnovers");
                return true;
            case R.id.goal_button:
                playerStats.setMode(3);
                setTitle("Current Mode: Goals");
                return true;
            case R.id.point_button:
                playerStats.setMode(4);
                setTitle("Current Mode: Points");
                return true;
            case R.id.wide_button:
                playerStats.setMode(5);
                setTitle("Current Mode: Wides");
                return true;
            case R.id.tackle_button:
                playerStats.setMode(6);
                setTitle("Current Mode: Tackles");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void incGoals(View w){
        playerStats.incScore(2);
        int score [] = playerStats.getScore();
        tv.setText(score[0] + "-" + score[1] + "\t\t\t\t\t\t\t\t" + score[2] + "-" + score[3]);
    }

    public void incPoints(View w){
        playerStats.incScore(3);
        int score [] = playerStats.getScore();
        tv.setText(score[0] + "-" + score[1] + "\t\t\t\t\t\t\t\t" + score[2] + "-" + score[3]);
    }

    public void updateScore(){
        playerStats.setScore(0,playerStats.totalMetric(playerStats.getGoals()));
        playerStats.setScore(1,playerStats.totalMetric(playerStats.getPoints()));
        tv.setText(playerStats.getScore()[0] + "-" + playerStats.getScore()[1] + "\t\t\t\t\t\t\t\t" +
                playerStats.getScore()[2] + "-" + playerStats.getScore()[3]);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Activity")
                .setMessage("Are you sure you want to exit? All your data will be lost.")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }
}

