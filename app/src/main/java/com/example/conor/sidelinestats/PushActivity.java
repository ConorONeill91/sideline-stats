package com.example.conor.sidelinestats;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;


import java.util.ArrayList;


public class PushActivity extends ActionBarActivity {

    private EditText et,ownerID;
    private Button submitButton;
    private TextView tv;
    protected ArrayList<int []> metrics;
    private ImageView tick;

    public ArrayList<int[]> getMetrics() {
        return metrics;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push);
        setTitle("Enter Unique Match Identifier / User ID to Sync Data");
        et = (EditText) findViewById(R.id.push_Text_Box);
        submitButton = (Button) findViewById(R.id.push_button);
        tv = (TextView) findViewById(R.id.push_textview);
        tick = (ImageView) findViewById(R.id.tickPush);
        ownerID = (EditText) findViewById(R.id.push_owner_box);
        tick.setVisibility(View.GONE);

        Intent intent = getIntent();
        int[] conversions = intent.getIntArrayExtra("Conversions");
        int[] turnovers = intent.getIntArrayExtra("Turnovers");
        int[] goals = intent.getIntArrayExtra("Goals");
        int[] points = intent.getIntArrayExtra("Points");
        int[] wides = intent.getIntArrayExtra("Wides");
        int[] tackles = intent.getIntArrayExtra("Tackles");

        int[] scores = intent.getIntArrayExtra("Score");

        metrics = new ArrayList<>();
        metrics.add(conversions);
        metrics.add(turnovers);
        metrics.add(goals);
        metrics.add(points);
        metrics.add(wides);
        metrics.add(tackles);
        metrics.add(scores);


    }

    public void submitData(View w) {
        sendData(metrics);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_push, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendData(ArrayList<int []> metrics){
        new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                // Pre Code
            }
            protected Void doInBackground(Void... unused) {
                String [] titles = {"Conversions", "Turnovers","Goals","Points","Wides","Tackles","Scores"};
                ArrayList<int []> metrics = getMetrics();
                ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
                postParameters.add(new BasicNameValuePair("UMI",et.getText().toString()));
                postParameters.add(new BasicNameValuePair("owner_id",ownerID.getText().toString()));
                for(int i = 0; i < metrics.size(); i++){
                    for(int j = 0; j < metrics.get(i).length; j++) {
                        postParameters.add(new BasicNameValuePair(titles[i] + j, metrics.get(i)[j] + ""));
                    }
                }

                String response = null;
                try {
                    response = CustomHttpClient.executeHttpPost("http://sidelinestats.net/api/match_data", postParameters);
                    String res = response.toString();
                    res = res.replaceAll("\\s+", "");

                    tick.setVisibility(View.VISIBLE);
                    tv.setText("Sync Complete");

                }
                catch (Exception e) {
                    Log.e("SLS", "HTTP Error");
                }
                return null;
            }

        }.execute();



    }
}
