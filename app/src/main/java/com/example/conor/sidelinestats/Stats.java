package com.example.conor.sidelinestats;

/**
 * Created by conor on 23/02/15.
    Model class for statistical information
 */
public class Stats  {
    private final int TEAMSIZE = 15;
    private int[] conversions; // 1
    private int[] turnovers; // 2
    private int[] goals; // 3
    private int[] points; // 4
    private int[] wides; // 5
    private int[] tackles; // 6
    private int currentMode;
    private String teamName;
    private int mostRecentInput;
    private int [] score; // Home Goals, Home Points, Away Goals, Away Points [0,1,2,3] Respectively

    public Stats() {
        conversions = new int[TEAMSIZE];
        turnovers = new int[TEAMSIZE];
        goals = new int[TEAMSIZE];
        points = new int[TEAMSIZE];
        wides = new int[TEAMSIZE];
        tackles = new int[TEAMSIZE];
        currentMode = 1; // conversions = initial mode @ Activity start.
        score = new int [5];
    }

    public void inc(int mode, int index) {
        setMostRecentInput(index);
        switch (mode) {
            case 1:
                conversions[index]++;
                break;
            case 2:
                turnovers[index]++;
                break;
            case 3:
                goals[index]++;
                break;
            case 4:
                points[index]++;
                break;
            case 5:
                wides[index]++;
                break;
            case 6:
                tackles[index]++;
                break;
        }
    }

    public int getMode() {
        return currentMode;
    }

    public void setMode(int newMode) {
        currentMode = newMode;
    }

    public int[] getConversions() {
        return conversions;
    }

    public int[] getTurnovers() {
        return turnovers;
    }

    public int[] getGoals() {
        return goals;
    }

    public int[] getPoints() {
        return points;
    }

    public int[] getWides() {
        return wides;
    }

    public int[] getTackles() {
        return tackles;
    }

    public int getMostRecentInput() {
        return mostRecentInput;
    }

    public void setMostRecentInput(int playerNumber) {
        mostRecentInput = playerNumber;
    }

    public int [] getScore(){
        return score;
    }

    public void setScore(int index, int value){
        score[index] = value;
    }

    public void incScore(int index){
        score[index]++;
    }

    public void undo(int mostRecentInput) {
        int mode = getMode();
        switch (mode) {
            case 1:
                if (conversions[mostRecentInput] > 0)
                    conversions[mostRecentInput]--;
                break;
            case 2:
                if (turnovers[mostRecentInput] > 0)
                    turnovers[mostRecentInput]--;
                break;
            case 3:
                if (goals[mostRecentInput] > 0)
                    goals[mostRecentInput]--;
                break;
            case 4:
                if (points[mostRecentInput] > 0)
                    points[mostRecentInput]--;
                break;
            case 5:
                if (wides[mostRecentInput] > 0)
                    wides[mostRecentInput]--;
                break;
            case 6:
                if (tackles[mostRecentInput] > 0)
                    tackles[mostRecentInput]--;
                break;
        }
    }

        public int totalMetric(int [] metric) {
            int total = 0;
            for(int i = 0; i < metric.length;i++ ){
                total += metric[i];
            }
            return total;
        }

        public void setResult() {
            if ((score[0] * 3 + score[1]) == (score[2] * 3 + score[3]))
                score[4] = 2;
            else if ((score[0] * 3 + score[1]) > (score[2] * 3 + score[3]))
                score[4] = 1;
            else
                score[4] = 0;
        }
}
