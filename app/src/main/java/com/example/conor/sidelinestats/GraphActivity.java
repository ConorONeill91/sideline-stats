package com.example.conor.sidelinestats;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.androidplot.Plot;
import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.BarRenderer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYStepMode;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GraphActivity extends Activity {

    private ListView lv;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        lv = (ListView) findViewById(R.id.listview);



        Intent intent = getIntent();
        int [] conversions = intent.getIntArrayExtra("Conversions");
        int [] turnovers = intent.getIntArrayExtra("Turnovers");
        int [] goals = intent.getIntArrayExtra("Goals");
        int [] points = intent.getIntArrayExtra("Points");
        int [] wides = intent.getIntArrayExtra("Wides");
        int [] tackles = intent.getIntArrayExtra("Tackles");

        ArrayList<int []>metrics = new ArrayList<>();
        metrics.add(conversions);
        metrics.add(turnovers);
        metrics.add(goals);
        metrics.add(points);
        metrics.add(wides);
        metrics.add(tackles);

        lv.setAdapter(new MyViewAdapter(getApplicationContext(), R.layout.listview_example_item, null, metrics));

    }


}

class MyViewAdapter extends ArrayAdapter<View> {


    private static class ViewHolder{
        private XYPlot plot;

    }


    private ArrayList<int []> items;

    public MyViewAdapter(Context context, int resId, List<View> views, ArrayList<int []> metrics) {
        super(context, resId, views);
        items = metrics;
    }

    @Override
    public int getCount() {
        return items.size();
    }




    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        LayoutInflater inf = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String[] titles = {"Conversions", "Turnovers", "Goals", "Points", "Wides", "Tackles"};
        int [] barColors = {Color.GREEN, Color.RED, Color.BLUE, Color.YELLOW, Color.CYAN, Color.MAGENTA};

        View v ;

        
        v = inf.inflate(R.layout.listview_example_item, parent, false);


        XYPlot p = (XYPlot) v.findViewById(R.id.mySimpleXYPlot);


        p.setTitle(titles[pos]);
        p.getLayoutManager().remove(p.getLegendWidget());

        p.getGraphWidget().getGridBackgroundPaint().setColor(Color.BLACK);
        p.getGraphWidget().getDomainGridLinePaint().setColor(Color.TRANSPARENT);
        p.getGraphWidget().getRangeGridLinePaint().setColor(Color.WHITE);
        p.getGraphWidget().setDrawMarkersEnabled(false);
        p.getGraphWidget().setGridPaddingRight(10);
        p.getGraphWidget().setGridPaddingLeft(10);

        p.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 1);
        p.setDomainValueFormat(new GraphXLabelFormat());
        p.getGraphWidget().setDomainLabelOrientation(45);


        p.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 1);
        p.setRangeBoundaries(0, 25, BoundaryMode.FIXED);
        p.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 5);
        p.setRangeValueFormat(new DecimalFormat("#"));


        Number[] series1Numbers = fillNumberArray(items.get(pos));


        XYSeries series1 = new SimpleXYSeries(Arrays.asList(series1Numbers),
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, titles[pos]);


        BarFormatter series1Format = new BarFormatter(barColors[pos], Color.WHITE);
        series1Format.setPointLabelFormatter(new PointLabelFormatter());


        // add a new series' to the xyplot:
        p.addSeries(series1, series1Format);

        p.redraw();


        return v;
    }



    public Number [] fillNumberArray(int [] values){
        Number [] nums = new Number[values.length];
        for(int i = 0; i < nums.length; i++){
            nums[i] = values[i];
        }
        return nums;
    }


}
// Format for X axis on graph
class GraphXLabelFormat extends Format {

    private String [] LABELS = {"GK", "LCB", "FB", "RCB", "LHB", "CB", "RHB", "MF", "MF","LHF","CF","RHF","LCF","FF","RCF"};

    @Override
    public StringBuffer format(Object object, StringBuffer buffer, FieldPosition field) {
        int parsedInt = Math.round(Float.parseFloat(object.toString()));
        String labelString = LABELS[parsedInt];

        buffer.append(labelString);
        return buffer;
    }

    @Override
    public Object parseObject(String string, ParsePosition position) {
        return java.util.Arrays.asList(LABELS).indexOf(string);
    }


}