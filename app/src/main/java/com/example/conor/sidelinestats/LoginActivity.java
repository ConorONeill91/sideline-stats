package com.example.conor.sidelinestats;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;


public class LoginActivity extends ActionBarActivity {

    private EditText un, pw;
    private Button submit;
    private TextView error;
    private boolean isLoggedIn;
    private ImageView tick;
    private int id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("SideLine Stats - Login to sync data with your account");
        isLoggedIn = false; // Start as false until logged in
        un = (EditText) findViewById(R.id.email_address);
        pw = (EditText) findViewById(R.id.password);
        submit = (Button) findViewById(R.id.submit);
        error = (TextView) findViewById(R.id.message);
        tick = (ImageView) findViewById(R.id.tick);

        tick.setVisibility(View.GONE);


        // Submit button handler
        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Async Task for Auth operation.
                new AsyncTask<Void, Void, Void>() {

                    protected Void doInBackground(Void... unused) {
                        ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
                        postParameters.add(new BasicNameValuePair("email", un.getText().toString()));
                        postParameters.add(new BasicNameValuePair("password", pw.getText().toString()));

                        //String valid = "1";
                        String response = null;
                        try {
                            response = CustomHttpClient.executeHttpPost("http://sidelinestats.net/api/auth", postParameters);  //Enetr Your remote PHP,ASP, Servlet file link
                            String res = response.toString();
                            res = res.replaceAll("\\s+", "");
                            if (res.equals("1")) {
                                error.setText("");
                                isLoggedIn = true;
                                tick.setVisibility(View.VISIBLE);
                                // id = Integer.parseInt("" + res.charAt(2));
                                Log.d("SLS_ID", "" + id);
                            } else {
                                error.setText("Incorrect Username or Password");
                                isLoggedIn = false;
                                tick.setVisibility(View.GONE);
                            }

                        } catch (Exception e) {
                            un.setText(e.toString());
                        }
                        return null;
                    }

                    protected void onPostExecute(Void unused) {
                        // Post Code
                    }
                }.execute();



            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.new_match_button) {
            if (isLoggedIn) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("Logged In", isLoggedIn);
                startActivity(intent);
                return true;
            } else {
                Intent intent1 = new Intent(this, MainActivityNotLoggedIn.class);
                intent1.putExtra("Logged In", isLoggedIn);
                startActivity(intent1);
                return true;
            }
        }


        return super.onOptionsItemSelected(item);
    }

}


